#!/bin/bash

set -e

help() {
cat << EOF
Simple Tmux Sharing

Usage:
    sts <command>

Commands:
    new <name>  -- creates a new stshare session
    join <name> -- join an existing stshare session
    kill <name> -- kill a particular session
    kill-all    -- kill all tmux sessions you control.
    help        -- show this screen
EOF
}

new() {
    session=$1
    hash=$(echo "$session" | md5sum | head -c 6)
    tmux -S /tmp/sts-"$hash" new -s "$session"
    chmod a+rw /tmp/sts-"$hash"
}

join() {
    session=$1
    hash=$(echo "$session" | md5sum | head -c 6)
    tmux -S /tmp/sts-"$hash" attach -t "$session"
}

kill_session() {
    session=$1
    hash=$(echo "$session" | md5sum | head -c 6)
    tmux -S /tmp/sts-"$hash" kill-session -t "$session"

}

command=$1

if [[ $command != "help" ]] \
    && [[ $command != "kill-all" ]] \
    && [[ $2 == "" ]] ; then

        echo "missing final argument. See usage below."
        echo
        help
        exit 
fi

case $command in
    "new")
        new "$2"
        ;;
    "join")
        join "$2"
        ;;
    "help")
        help
        ;;
    "kill")
        kill_session "$2"
        ;;
    "kill-all")
        tmux kill-server
        ;;
    "*")
        echo "Invalid command: $2"
        help
        ;;
esac
