# **S**imple **T**mux **S**haring

An ultra-simple bash script that simplifies the process of pair programming using tmux and ssh.

It passes shellcheck, so it must be bug-free, right??

## Rationale:

I wanted to be able to program with friends and colleagues, but found screensharing wasn't quite enough (not easy enough to share control between participants) and didn't want to use another editor with plugins that support pairing.

I was familiar with the fact that tmux COULD be used to share terminal sessions among multiple users, but I could never remember the syntax.

So, sts simply aliases the correct tmux commands, as well as manages a little housekeeping, like generating the socket name.

## **Warning:**

* sts is a minimally tested bash script. Be advised that behavior isn't necessarily consistent or bug-proof (in fact, I'm sure it's not bug proof, so it shouldn't be hard to break if you try).
* **Anyone who joins your session can perform actions as your user.** As a result, you'll want to ensure that you shut down your sessions once you are done. This can be done with `sts kill <session-name>`. You can also run `sts kill-all` to kill all the sessions that you have access to.
